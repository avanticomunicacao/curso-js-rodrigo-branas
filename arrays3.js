var carros = [];
 
carros[0] = {modelo: "Ka", preco: 28800};
carros[1] = {modelo: "Corsa", preco: 34750};
carros[2] = {modelo: "Palio", preco: 32000};

var total = carros.reduce((acumulado, carro) => acumulado + carro.preco, 0);

console.log(total);