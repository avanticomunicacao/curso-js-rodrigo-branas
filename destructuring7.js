let carros = [
	{marca: "Fiat", modelo: "Uno", cor: "branca"},
	{marca: "Ford", modelo: "Ka", cor: "verde"},
	{marca: "Honda", modelo: "Civic"}
];

for (let {marca, cor = 'Indefinida'} of carros) {
	console.log(`O carro da marca ${marca} é ${cor}`);
}