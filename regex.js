var regexp = /^[a-z0-9_\.]+@[a-z0-9_]+(\.[a-z0-9]+)+$/;
console.log("" + regexp.test("")); // false
console.log("rodrigo.branas " + regexp.test("rodrigo.branas")); // false
console.log("rodrigobranasgmail.com " + regexp.test("rodrigobranasgmail.com")); // false
console.log("rodrigo.branasgmail.com " + regexp.test("rodrigo.branasgmail.com")); // false
console.log("rodrigo.branas@gmail " + regexp.test("rodrigo.branas@gmail")); // false
console.log("rodrigo@gmail.com " + regexp.test("rodrigo@gmail.com"));
console.log("rodrigo@gmail.net " + regexp.test("rodrigo@gmail.net"));
console.log("rodrigo.branas@gmail.com " + regexp.test("rodrigo.branas@gmail.com"));
console.log("rodrigo.branas@gmail.com.br " + regexp.test("rodrigo.branas@gmail.com.br"));

var regexp2 = /[a-z0-9_\.]+@[a-z0-9_]+(\.[a-z0-9]+)+/gim;

var emails = "<tr><td>RODRIGO.branas@gmail.com</td></tr> \
              <tr><td>rodrigo.branas@agilecode.com.br</td></tr>";

console.log(emails.match(regexp2));