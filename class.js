class Shape {
	constructor(type) {
		this.type = type;
	}

	get area() {
		return this.type;
	}

	static helloWorld() {
		console.log("Hello World!");
	}
}

class Rect extends Shape {
	constructor(width, height) {
		super("Rect");
		this.width = width;
		this.height = height;
	}

	get area() {
		return super.area + " " + this.width * this.height;
	}
}

class Circle extends Shape {
	constructor(radius) {
		super("Circle");
		this.radius = radius;
	}

	get area() {
		return super.area + " " + Math.pow(this.radius, 2) * Math.PI;
	}
}

class Triangle extends Shape {
	constructor(x, y, z) {
		super("Triangle")
	}
}

var rect = new Rect(10, 20);
console.log(rect.area);

var circle = new Circle(10);
console.log(circle.area);

Circle.helloWorld();