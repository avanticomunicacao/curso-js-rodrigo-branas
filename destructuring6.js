var pessoa = {
	nome: "Ana Maria",
	idade: 30,
	endereco: {
		rua: "Av. Brasil",
		numero: 10000
	}
};

var {idade: a, 
	nome: b, 
	endereco: {
		rua: c, 
		numero: d
	}
} = pessoa;

console.log(a);
console.log(b);
console.log(c);
console.log(d);