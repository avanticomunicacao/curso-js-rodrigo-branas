var message = "A";
var helloWorld = function () {
	var message = "B";
	return function () {
		return message;
	};
};

var fn = helloWorld();
// 10 horas depois...
console.log(fn());