var idade = 25;

var getIdade = function () {
	console.log(this.idade);
};

var maria = {
	idade: 12
};

var joao = {
  idade: 10,
  getIdade: getIdade
};

getIdade.call(maria);
joao.getIdade.call(maria);
joao.getIdade();
getIdade();
console.log(getIdade === joao.getIdade);







