var Homem = function (nome, idade) {
	this.nome = nome;
	this.idade = idade;
};

Homem.prototype.sexo = "masculino";

var joao = new Homem("João", 20);

console.log(joao);
console.log(joao.sexo);