var frutas = ["morango", "banana", "laranja", "abacaxi", "manga"];
  
var saladaDeFrutas = {};

for(var i = 0; i < (frutas.length - 1); i++) {
    saladaDeFrutas[frutas[i]] = (function () {
        return "Meu nome é " + this.fruta;
    }).bind({fruta: frutas[i]});
}
  
console.log(saladaDeFrutas.morango());
console.log(saladaDeFrutas.banana());
console.log(saladaDeFrutas.laranja());
console.log(saladaDeFrutas.abacaxi());