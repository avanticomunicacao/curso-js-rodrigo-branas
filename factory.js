var homem = {
	sexo: "masculino"
};

var criarHomem = function (nome, idade) {
	var pessoa = {
		nome: nome,
		idade: idade
	};
	Object.setPrototypeOf(pessoa, homem);
	return pessoa;
};

var pedro = criarHomem("Pedro", 22);

console.log(pedro);
console.log(pedro.sexo);