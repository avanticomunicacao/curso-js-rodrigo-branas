var Homem = function (nome, idade) {
	this.nome = nome;
	this.idade = idade;
};

Homem.prototype.sexo = "masculino";

Homem.prototype.getNome = function () {
	return this.nome;
};

Homem.prototype.getIdade = function () {
	return this.idade;
};

var marcos = new Homem("Marcos", 22);
console.log(marcos.getNome());
console.log(marcos.getIdade());
console.log(marcos.sexo);

console.log(marcos instanceof Homem);
console.log(marcos instanceof Object);
console.log(marcos instanceof Date);

var pedro = Object.create(Homem.prototype);
Homem.call(pedro, "Pedro", 20);
console.log(pedro.getNome());
console.log(pedro.getIdade());
console.log(pedro.sexo);



