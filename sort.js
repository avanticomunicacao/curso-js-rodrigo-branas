var carros = [];
 
carros[0] = {modelo: "c", preco: 70000};  
carros[1] = {modelo: "a", preco: 28800};
carros[2] = {modelo: "b", preco: 34750};
carros[3] = {modelo: "á", preco: 32000};

carros.sort(function (a, b) {
  return a.modelo.localeCompare(b.modelo);
});

console.log(carros);