//console.log(Array.prototype.slice.call(arguments, 1));

var Homem = function (nome, idade) {
	this.nome = nome;
	this.idade = idade;
	return null;
};

Homem.prototype.sexo = "masculino";

function _new(fn) {
	arguments.__proto__ = Array.prototype;
	var obj = {};
	Object.setPrototypeOf(obj, fn.prototype);
	var ret = fn.apply(obj, arguments.slice(1));
	if (ret && (typeof ret === 'object')) return ret;
	return obj;
}

var joao = _new(Homem, "João", 20);
console.log(joao);
console.log(joao.sexo);

console.log(joao instanceof Homem);
console.log(joao instanceof Object);
console.log(joao instanceof Date);
console.log(joao instanceof RegExp);









