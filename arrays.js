class Carro {
	constructor(marca, modelo) {
		this.marca = marca;
		this.modelo = modelo;
	}
}

var carros = [
	new Carro("Fiat", "Uno"),
	new Carro("Honda", "Civic"),
	new Carro("Ford", "Ka")
];

for(var i = 0; i < carros.length; i++) {
	var carro = carros[i];
	console.log(carro);
}

carros.forEach(function (carro) {
	console.log(carro);
});

for(var carro of carros) {
	console.log(carro);
}







