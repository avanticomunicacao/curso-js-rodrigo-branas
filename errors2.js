class Exception {
	constructor(message) {
		this.message = message;
	}
}

class CarroException extends Exception {
	constructor(message, parameter) {
		super(message);
		this.parameter = parameter;
	}
}

class Carro {
	constructor ({marca, modelo, cor}) {
		if (!marca || !modelo || !cor) {
			throw new Error("O carro não pode ser criado porque faltam parametros.");
		}
		this.marca = marca;
		this.modelo = modelo;
		this.cor = cor;
		this.opcionais = [];
	}

	adicionarOpcional (opcional) {
		if (!opcional) {
			throw new CarroException("O opcional precisa ter a descrição");
		}
		this.opcionais.push(opcional);
	}
}

var params = {
	marca: "Fiat",
	modelo: "Uno",
	cor: "branca"
};

var uno;
try {
	uno = new Carro(params);
	uno.adicionarOpcional("alarme");
	uno.adicionarOpcional("som");
	uno.adicionarOpcional();
} catch (carroException) {
	console.log(carroException.message);
}
console.log(uno);