var carros = [];
 
carros[0] = {modelo: "Uno", preco: 10000};
carros[1] = {modelo: "Ka", preco: 28800};
carros[2] = {modelo: "Fusion", preco: 70000};
carros[3] = {modelo: "Corsa", preco: 34750};
carros[4] = {modelo: "Palio", preco: 32000};

carros.sort(function (a, b) {
	return a.modelo.localeCompare(b.modelo);
});

console.log(carros);