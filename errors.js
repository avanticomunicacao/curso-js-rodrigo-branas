function soma (a, b) {
	if (!a || !b) throw new Error("Por favor, informe os parâmetros corretamente.");
	return a + b;
} 

soma(2,2);
try {
	soma(2);
} catch (e) {
	console.log(e);
}