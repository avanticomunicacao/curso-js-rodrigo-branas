class Shape {
	constructor(type) {
		this.type = type;
	}

	getType() {
		return this.type
	}
}

class Rect extends Shape {
	constructor(a, b) {
		super("Rect");
		this.a = a;
		this.b = b;
	}

	static calculateArea(a, b) {
		return a * b;
	}
}

console.log(Rect.calculateArea(2,3));







