function soma ({x: a, z: b}) {
	return a + b;
}

var params = {
	x: 2,
	z: 2
};

console.log(soma(params));