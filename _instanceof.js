//console.log(Array.prototype.slice.call(arguments, 1));

var Homem = function (nome, idade) {
	this.nome = nome;
	this.idade = idade;
	return null;
};

Homem.prototype.sexo = "masculino";

function _new(fn) {
	arguments.__proto__ = Array.prototype;
	var obj = {};
	Object.setPrototypeOf(obj, fn.prototype);
	var ret = fn.apply(obj, arguments.slice(1));
	if (ret && (typeof ret === 'object')) return ret;
	return obj;
}

var joao = _new(Homem, "João", 20);
console.log(joao);
console.log(joao.sexo);

function instanceOf(obj, fn) {
	if (obj === null) return false;
	if (obj === fn.prototype) return true;
	return instanceOf(obj.__proto__, fn);
}

console.log(instanceOf(joao, Homem));
console.log(instanceOf(joao, Object));
console.log(instanceOf(joao, Array));
console.log(instanceOf(joao, Date));









