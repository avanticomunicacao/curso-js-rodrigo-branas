class Carro {
	constructor(marca, modelo) {
		this.marca = marca;
		this.modelo = modelo;
	}
}

var carros = [
	new Carro("Fiat", "Uno"),
	new Carro("Honda", "Civic"),
	new Carro("Ford", "Ka")
];

var forEach = function (array, fn) {
	for(var i = 0; i < array.length; i++) {
		fn(array[i]);
	}
};

forEach(carros, function (carro) {
	carro.novo = true;
	console.log(carro);
});