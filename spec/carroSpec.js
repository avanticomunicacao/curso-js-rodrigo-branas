describe("Carro Spec", function () {

	it("2 - Deve testar o carro", function () {
		var carro = {
			marca: "fiat",
			modelo: "uno",
			cor: "vermelho"
		};
		expect(carro.marca).toBe("fiat");
	});

	it("3 A - Deve testar o veiculo com __proto__", function () {
		var veiculo = {
			marca: "fiat",
			modelo: "uno",
			cor: "vermelho"
		};

		var carro = {
			numeroDePortas: 4,
			temArcondicionado: true,
			__proto__: veiculo
		};

		expect(carro.marca).toBe("fiat");
	});

	it("3 B - Deve testar o veiculo com setPrototypeOf", function () {
		var veiculo = {
			marca: "fiat",
			modelo: "uno",
			cor: "vermelho"
		};

		var carro = {
			numeroDePortas: 4,
			temArcondicionado: true
		};
		Object.setPrototypeOf(carro, veiculo);
		expect(carro.marca).toBe("fiat");
	});

	it("3 C - Deve testar o veiculo com Object.create", function () {
		var veiculo = {
			marca: "fiat",
			modelo: "uno",
			cor: "vermelho"
		};

		var carro = Object.create(veiculo);
		carro.numeroDePortas = 4;
		carro.temArcondicionado = true;

		expect(carro.marca).toBe("fiat");
	});

	it("4 - Deve retornar a marca e modelo do carro ao invocar a função por meio do objeto", function () {
		var carro = {
			marca: "Fiat",
			modelo: "Uno"
		};

		var mostrarMarcaModelo = function (carro) {
			return carro.marca + " " + carro.modelo;
		};

		expect(mostrarMarcaModelo(carro)).toBe("Fiat Uno");
	});

	it("5 - Deve retornar a marca e modelo do carro ao invocar a função por meio do objeto", function () {
		var carro = {
			marca: "Fiat",
			modelo: "Uno",
			getMarcaModelo: function () {
				return this.marca + " " + this.modelo;
			}
		};

		expect(carro.getMarcaModelo()).toBe("Fiat Uno");
	});

	it("6 - Deve retornar a marca e modelo do carro ao invocar a função por meio da função call/apply", function () {
		var getMarcaModelo = function () {
			return this.marca + " " + this.modelo;
		};

		var carro = {
			marca: "Fiat",
			modelo: "Uno"
		};

		expect(getMarcaModelo.call(carro)).toBe("Fiat Uno");
	});

	it("7 - Deve testar o carro por meio da função fábrica", function () {
		var criarCarro = function (x, y, z) {
			return {
				marca: x,
				modelo: y,
				cor: z
			};
		};
		var carro = criarCarro("fiat", "uno", "vermelho");
		expect(carro.marca).toBe("fiat");
	});

	it("8 - Deve testar o carro por meio da função construtora", function () {
		var Carro = function (x, y, z) {
			this.marca = x;
			this.modelo = y;
			this.cor = z;
		};
		var carro = new Carro("fiat", "uno", "vermelho");
		expect(carro.marca).toBe("fiat");
		expect(carro.modelo).toBe("uno");
		expect(carro.cor).toBe("vermelho");
	});

	it("10 - Implementar o new", function () {
		var Veiculo = function (marca, modelo) {
			this.marca = marca;
			this.modelo = modelo;
		};

		var _new = function (fn) {
			arguments.__proto__ = Array.prototype;
			var obj = {};
			fn.apply(obj, arguments.slice(1));
			return obj;
		};
		var veiculo = _new(Veiculo, "Fiat", "Uno");
		expect(veiculo.marca).toBe("Fiat");
		expect(veiculo.modelo).toBe("Uno");
	});

	it ("11 - Implementar o instanceof", function () {
		var Veiculo = function (marca, modelo) {
			this.marca = marca;
			this.modelo = modelo;
		};

		var _instanceof = function (obj, fn) {
			if (obj === null) return false;
			if (obj === fn.prototype) return true;
			return _instanceof(obj.__proto__, fn);
		};

		var veiculo = new Veiculo("Fiat", "Uno");

		expect(_instanceof(veiculo, Veiculo)).toBe(true);
		expect(_instanceof(veiculo, Array)).toBe(false);
	});

	it ("15 - Criar a classe Veiculo, contendo as propriedades marca, modelo, placa e cor", function () {
		class Veiculo {
			constructor(tipo, marca, modelo, placa, cor) {
				this.tipo = tipo;
				this.marca = marca;
				this.modelo = modelo;
				this.placa = placa;
				this.cor = cor;
			}
		}

		var veiculo = new Veiculo("Carro", "Fiat", "Uno", "AAA9999", "vermelho");

		expect(veiculo instanceof Veiculo).toBe(true);
		expect(veiculo.marca).toBe("Fiat");
		expect(veiculo.modelo).toBe("Uno");
		expect(veiculo.placa).toBe("AAA9999");
		expect(veiculo.cor).toBe("vermelho");

	});

	it ("16 - Criar a classe Carro e Moto, contendo a propriedade portas", function () {
		class Veiculo {
			constructor(tipo, marca, modelo, placa, cor) {
				this.tipo = tipo;
				this.marca = marca;
				this.modelo = modelo;
				this.placa = placa;
				this.cor = cor;
			}
		}

		class Carro extends Veiculo {
			constructor(marca, modelo, placa, cor, portas) {
				super("Carro", marca, modelo, placa, cor);
				this.portas = portas;
			}
		}

		var carro = new Carro("Fiat", "Uno", "AAA9999", "vermelho", 2);

		expect(carro instanceof Veiculo).toBe(true);
		expect(carro instanceof Carro).toBe(true);
		expect(carro.marca).toBe("Fiat");
		expect(carro.modelo).toBe("Uno");
		expect(carro.placa).toBe("AAA9999");
		expect(carro.cor).toBe("vermelho");
		expect(carro.portas).toBe(2);
		
		class Moto extends Veiculo {
			constructor(marca, modelo, placa, cor, cilindrada) {
				super("Moto", marca, modelo, placa, cor);
				this.cilindrada = cilindrada;
			}
		}

		var moto = new Moto("Yamaha", "Tornado", "BBB9999", "branca", 250);

		expect(moto instanceof Veiculo).toBe(true);
		expect(moto instanceof Moto).toBe(true);
		expect(moto.marca).toBe("Yamaha");
		expect(moto.modelo).toBe("Tornado");
		expect(moto.placa).toBe("BBB9999");
		expect(moto.cor).toBe("branca");
		expect(moto.cilindrada).toBe(250);

	});

	it ("17 - Criar um método para exibir as propriedades do Carro e da Moto", function () {
		class Veiculo {
			constructor(tipo, marca, modelo, placa, cor) {
				this.tipo = tipo;
				this.marca = marca;
				this.modelo = modelo;
				this.placa = placa;
				this.cor = cor;
			}

			getDetalhes() {
				return `${this.marca} ${this.modelo} ${this.placa} ${this.cor}`;
			}

			static calcularConsumo(distancia, litros) {
				return distancia/litros;
			}
		}

		class Carro extends Veiculo {
			constructor(marca, modelo, placa, cor, portas) {
				super("Carro", marca, modelo, placa, cor);
				this.portas = portas;
			}

			set airbags (airbags) {
				if (airbags < 0) {
					console.log("O número de airbags deve ser positivo.");
					return;
				}
				this._airbags = airbags;
			}

			getDetalhes() {
				return `${super.getDetalhes()} ${this.portas} portas`;
			}
		}

		var carro = new Carro("Fiat", "Uno", "AAA9999", "vermelho", 2);
		carro.airbags = -7;
		expect(carro.getDetalhes()).toBe("Fiat Uno AAA9999 vermelho 2 portas");
		console.log(carro);

		class Moto extends Veiculo {
			constructor(marca, modelo, placa, cor, cilindrada) {
				super("Moto", marca, modelo, placa, cor);
				this.cilindrada = cilindrada;
			}

			getDetalhes() {
				return `${super.getDetalhes()} ${this.cilindrada} cilindradas`;
			}
		}

		var moto = new Moto("Yamaha", "Tornado", "BBB9999", "branca", 250);

		expect(moto.getDetalhes()).toBe("Yamaha Tornado BBB9999 branca 250 cilindradas");

		expect(Veiculo.calcularConsumo(120, 10)).toBe(12);
	});

	it("18 - Criar dentro da carro, um array de 'opcionais', inicializando com 3 opcionais: 'Vidro Elétrico, Alarme e Som'.", function () {
		class Carro {
			constructor(marca, modelo) {
				this.marca = marca;
				this.modelo = modelo;
				this.opcionais = ["Vidro Elétrico", "Alarme", "Som"];
			}

			getOpcionais() {
				return this.opcionais;
			}
		}
		var carro = new Carro("Fiat", "Uno");
		expect(carro.getOpcionais()[0]).toBe("Vidro Elétrico");
	});

	it("19 - Exibir os opcionais.", function () {
		class Carro {
			constructor(marca, modelo) {
				this.marca = marca;
				this.modelo = modelo;
				this.opcionais = ["Vidro Elétrico", "Alarme", "Som"];
			}

			getOpcionais() {
				return this.opcionais;
			}

			exibirOpcionais() {
				for(var opcional of this.opcionais) {
					console.log(opcional);
				}
			}
		}
		var carro = new Carro("Fiat", "Uno");
		carro.exibirOpcionais();
	});

	it("20 - Criar um médoto para adicionar e remover um ou mais opcionais.", function () {
		class Carro {
			constructor(marca, modelo) {
				this.marca = marca;
				this.modelo = modelo;
				this.opcionais = [];
			}

			getOpcionais() {
				return this.opcionais;
			}

			adicionarOpcional(...opcionais) {
				this.opcionais.push(...opcionais);
			}

			removerOpcional(...opcionais) {
				this.opcionais = this.opcionais.filter(function (opcional) {
					return opcionais.indexOf(opcional) === -1;
				});
			}
		}
		var carro = new Carro("Fiat", "Uno");
		carro.adicionarOpcional("Vidro Elétrico", "Alarme", "Som");
		expect(carro.getOpcionais()[0]).toBe("Vidro Elétrico");
		expect(carro.getOpcionais()[1]).toBe("Alarme");
		expect(carro.getOpcionais()[2]).toBe("Som");
		carro.removerOpcional("Alarme", "Som");
		expect(carro.getOpcionais()[0]).toBe("Vidro Elétrico");
		expect(carro.getOpcionais()[1]).toBe(undefined);
		expect(carro.getOpcionais()[2]).toBe(undefined);
	});

	it ("23 - Os opcionais devem ter descrição e preço.", function () {
		class Opcional {
			constructor(descricao, preco) {
				this.descricao = descricao;
				this.preco = preco;
			}
		}

		class Carro {
			constructor(marca, modelo, opcionais) {
				this.marca = marca;
				this.modelo = modelo;
				this.opcionais = opcionais;
			}

			getOpcional(descricao) {
				return this.opcionais.find(opcional => opcional.descricao === descricao);
			}

			hasOpcional(descricao) {
				return this.opcionais.some(opcional => opcional.descricao === descricao);
			}

			getOpcionais() {
				return this.opcionais;
			}
		}

		var opcionais = [
			new Opcional("Vidro Elétrico", 1000),
			new Opcional("Alarme", 600),
			new Opcional("Som", 500)
		];
		var uno = new Carro("Fiat", "Uno", opcionais);
		expect(uno.getOpcional("Som").preco).toBe(500);
		expect(uno.hasOpcional("Alarme")).toBe(true);
	});

	it("30 - Ordenar os opcionais.", function () {
		class Opcional {
			constructor(descricao, preco) {
				this.descricao = descricao;
				this.preco = preco;
			}
		}

		class Carro {
			constructor(marca, modelo, opcionais) {
				this.marca = marca;
				this.modelo = modelo;
				this.opcionais = opcionais;
			}

			order(campo, direcao) {
				var dir = (direcao === "asc") ? 1 : -1;
				this.opcionais.sort(function (a, b) {
					if (a[campo] < b[campo]) return -1 * dir;
					if (a[campo] > b[campo]) return 1 * dir;
					return 0;
				});
			}
		}

		var opcionais = [
			new Opcional("Vidro Elétrico", 500),
			new Opcional("Alarme", 100),
			new Opcional("Som", 800)
		];
		var carro = new Carro("Fiat", "Uno", opcionais);
		carro.order("preco", "asc");
		expect(carro.opcionais[0].descricao).toBe("Alarme");
		expect(carro.opcionais[1].descricao).toBe("Vidro Elétrico");
		expect(carro.opcionais[2].descricao).toBe("Som");
		carro.order("preco", "desc");
		expect(carro.opcionais[0].descricao).toBe("Som");
		expect(carro.opcionais[1].descricao).toBe("Vidro Elétrico");
		expect(carro.opcionais[2].descricao).toBe("Alarme");
		carro.order("descricao", "asc");
		expect(carro.opcionais[0].descricao).toBe("Alarme");
		expect(carro.opcionais[1].descricao).toBe("Som");
		expect(carro.opcionais[2].descricao).toBe("Vidro Elétrico");
		carro.order("descricao", "desc");
		expect(carro.opcionais[0].descricao).toBe("Vidro Elétrico");
		expect(carro.opcionais[1].descricao).toBe("Som");
		expect(carro.opcionais[2].descricao).toBe("Alarme");
	});
});

















