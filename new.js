function _new(fn) {
	var obj = {};
	obj.__proto__ = fn.prototype;
	fn.apply(obj, Array.prototype.slice.call(arguments, 1));
	return obj;
}

var Homem = function (nome, idade) {
	this.nome = nome;
	this.idade = idade;
};

Homem.prototype.sexo = "masculino";

var pedro = _new(Homem, "Pedro", 20);
console.log(pedro);
console.log(pedro.sexo);

console.log(pedro instanceof Homem);